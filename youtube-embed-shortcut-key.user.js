// ==UserScript==
// @name        YouTube embed shortcut key
// @namespace   kuni93.moe
// @version     1.0.1
// @author      TzuLum Yen <tzulumyen@kuni93.moe>
// @description Press "e" to open YouTube embed player

// @match        https://www.youtube.com/*
// @grant       none
// ==/UserScript==

(function() {
    'use strict';

    document.addEventListener('keyup', function(event) {
        const youtubeVideoId = new URL(window.location.href).searchParams.get("v");

        if (youtubeVideoId === null) {
            return;
        }
        if ('input' === document.activeElement.tagName.toLowerCase()) {
            return;
        }
        if ('e' !== event.key) {
            return;
        }

        document.location = 'https://www.youtube-nocookie.com/embed/' + youtubeVideoId;
    });
})();
